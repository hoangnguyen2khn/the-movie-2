const colors = require("tailwindcss/colors");
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    colors: {
      ...colors,
      red: {
        ...colors.red,
        main: "#E50913",
      },
    },
    extend: {
      padding: {
        container: "32px",
      },
    },
  },
  plugins: [],
};
