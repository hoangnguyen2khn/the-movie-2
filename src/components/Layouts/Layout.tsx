import React, { ReactNode } from 'react'
import Link from 'next/link'

type Props = {
  children?: ReactNode
  title?: string
}

const Layout = ({ children, title = 'This is the default title' }: Props) => (
  <div>
    
    
    {children}
   
  </div>
)

export default Layout
