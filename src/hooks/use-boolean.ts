import { useCallback, useState } from 'react';

export function useBoolean(initialState: boolean): [boolean, () => void, () => void, () => void] {
  const [value, setValue] = useState(initialState);

  const setTrue = useCallback(() => setValue(true), []);
  const setFalse = useCallback(() => setValue(false), []);
  const toggle = useCallback(() => setValue((v) => !v), []);

  return [value, setTrue, setFalse, toggle];
}
