export const routeBaseHome = '/';

export const routeBaseMovieCategory = '/[category]';

export const routeBaseMovieListCatalogue = '/[catalogue]';

export const routeBaseMovieDetails = '/[category]/[id]';